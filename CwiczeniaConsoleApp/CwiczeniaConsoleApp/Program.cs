﻿using CwiczeniaConsoleApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CwiczeniaConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Movie> movies = new List<Movie>();
            Movie movie1 = new Movie { Name = "The Godfather", Year = 1972, AgeLimit = 15, Genre = Genre.Crime };
            Movie movie2 = new Movie { Name = "Forrest Gump", Year = 1994, AgeLimit = 12, Genre = Genre.Drama };
            ChildMovie movie3 = new ChildMovie { Name = "Shrek", Year = 2001, AgeLimit = 7 };
            ChildMovie movie4 = new ChildMovie { Name = "The Lion King", Year = 1994, AgeLimit = 5 };
            ChildMovie movie5 = new ChildMovie { Name = "Inside out", Year = 2015, AgeLimit = 3 };
            movies.Add(movie1);
            movies.Add(movie2);
            movies.Add(movie3);
            movies.Add(movie4);
            movies.Add(movie5);

            Console.WriteLine("Enter movie name.");
            string movieName = Console.ReadLine();

            Console.WriteLine("Enter production year.");
            string year = Console.ReadLine();
            int yearNumber;
            while (!IsYearValid(year, out yearNumber))
            {
                Console.WriteLine("Enter production year.");
                year = Console.ReadLine();
            }

            Console.WriteLine("Year valid. Enter age limit");
            string ageLimit = Console.ReadLine();
            int ageLimitNumber;
            while (!IAgeLimitValid(ageLimit, out ageLimitNumber))
            {
                Console.WriteLine("Enter age limit");
                ageLimit = Console.ReadLine();
            }

            Console.WriteLine("Age limit valid. Enter genre");
            string genre = Console.ReadLine();
            Genre convertedGenre;
            //TryParse jest metodą wbudowaną w enum, która konwertuje stringa na enum. dodatkowym (opcjonalnym) parametrem jest tutaj ignorecase (u nas ustalony na true), tzn. przejdzie zarówno Crime jak i crime.
            while (!Enum.TryParse(genre, true, out convertedGenre))
            {
                Console.WriteLine("Enter genre");
                genre = Console.ReadLine();
            }
            Console.WriteLine("Genre valid.");

            Console.WriteLine("Enter child's birth year.");
            string birthYearString = Console.ReadLine();
            int birthYear;
            while (!Int32.TryParse(birthYearString, out birthYear))
            {
                Console.WriteLine("Enter child's birth year.");
                birthYearString = Console.ReadLine();
            }
            int childAge = DateTime.Now.Year - birthYear;
            List<string> names = movies.Where(m => CanBeWatchedByAPersonOfAge(childAge, m)).Select(m => m.Name).ToList();
            names.ForEach(n => { Console.WriteLine(n); });
            Console.ReadLine();

            //zapisywanie do pliku z serializacją do Jsona. Ważne: należy wybrać z paska Tools=>NugetPackageManager=>PackageManagerConsole i w okno PackageManagerConsole wpisać: Install-Package Newtonsoft.Json -Version 12.0.2
            using (StreamWriter writer = new StreamWriter("movies.txt"))
            {
                writer.WriteLine(JsonConvert.SerializeObject(movies));
            }

            using (StreamReader reader = new StreamReader("newMovie.txt"))
            {
                string newMovieJson = reader.ReadToEnd();
                Movie newMovie = JsonConvert.DeserializeObject<Movie>(newMovieJson);
                movies.Add(newMovie);
            }

            Console.WriteLine("Enter website of your new movie");
            string possibleUrl = Console.ReadLine();
            while (!IsStringUrl(possibleUrl))
            {
                Console.WriteLine("Enter website of your new movie");
                possibleUrl = Console.ReadLine();
            }
            Movie movie = new Movie();
            movie.Website = possibleUrl;
        }

        private static bool IsYearValid(string year, out int yearNumber)
        {
            if (!Int32.TryParse(year, out yearNumber))
            {
                return false;
            }

            return (yearNumber >= 1900 && yearNumber <= DateTime.Now.Year);
        }

        private static bool IAgeLimitValid(string ageLimit, out int ageLimitNumber)
        {
            if (!Int32.TryParse(ageLimit, out ageLimitNumber))
            {
                return false;
            }

            return (ageLimitNumber >= 0 && ageLimitNumber <= 100);
        }

        private static bool CanBeWatchedByAPersonOfAge(int age, Movie movie)
        {
            return age >= movie.AgeLimit;
        }

        private static bool IsStringUrl(string possibleUrl)
        {
            Regex regex = new Regex(@"^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&%\$#_]*)?$");
            bool isUrl = regex.IsMatch(possibleUrl);
            return isUrl;
        }
    }
}
