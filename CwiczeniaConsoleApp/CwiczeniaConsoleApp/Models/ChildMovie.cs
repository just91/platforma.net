﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CwiczeniaConsoleApp.Models
{
    class ChildMovie:Movie
    {
        public override Genre Genre { get { return Genre.Animated; } set { Genre = Genre.Animated; } }
    }
}
