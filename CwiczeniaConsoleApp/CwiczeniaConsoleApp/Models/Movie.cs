﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CwiczeniaConsoleApp.Models
{
    class Movie
    {
        public string Name { get; set; }
        public int Year { get; set; }
        public int AgeLimit { get; set; }

        //właściwość Genre jest virtual tzn. można ją nadpisać w podklasie
        public virtual Genre Genre { get; set; }

        public string Website { get; set; }
        public int GetMovieAge()
        {
            return DateTime.Now.Year - this.Year;
        }
        
    }
}
