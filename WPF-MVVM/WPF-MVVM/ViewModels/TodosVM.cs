﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WPF_MVVM.Models;

namespace WPF_MVVM.ViewModels
{
    public class TodosVM
    {
        public ObservableCollection<Todo> Todos { get; set; }
        public TodosVM()
        {
            Todos = new ObservableCollection<Todo>();
        }

        public void AddTodo(string name, string numberString, DateTime beginning)
        {
            int number;
            if (!Int32.TryParse(numberString, out number))
            {
                return;
            }
            Todos.Add(new Todo(name, number, beginning));
        }

        internal void AddParticipantToTodo(Todo selectedItem)
        {
            selectedItem.NumberOfParticipants++;
        }

        internal void DeleteTodo(Todo selectedItem)
        {
            if (Todos.Contains(selectedItem))
            {
                Todos.Remove(selectedItem);
            }
        }

        public async Task AddTodoFromWebAsync()
        {
            using (HttpClient client = new HttpClient())
            {
                // Call asynchronous network methods in a try/catch block to handle exceptions
                try
                {
                    HttpResponseMessage response = await client.GetAsync("http://tabor.faculty.wmi.amu.edu.pl/todo.html");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();
                    Todo webTodo = JsonConvert.DeserializeObject<Todo>(responseBody);
                    Todos.Add(webTodo);
                }
                catch (HttpRequestException e)
                {
                }
            }
        }
    }
}
