﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPF_MVVM.Models;
using WPF_MVVM.ViewModels;

namespace WPF_MVVM
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
       public TodosVM vm { get { return DataContext as TodosVM; }}
        public MainWindow()
        {
            InitializeComponent();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            vm.AddTodo(InputTodoName.Text, InputTodoNumberOfParticipants.Text, (DateTime)InputTodoBeginning.SelectedDate);
            InputTodoName.Text = "";
            InputTodoNumberOfParticipants.Text = "";
            InputTodoBeginning.SelectedDate = DateTime.Now;
        }

        private void AddOneParticipantButton_Click(object sender, RoutedEventArgs e)
        {
            vm.AddParticipantToTodo((Todo)(TodosListView.SelectedItem));
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            vm.DeleteTodo((Todo)(TodosListView.SelectedItem));
        }

        private async void AddButtonFromWeb_Click(object sender, RoutedEventArgs e)
        {
            await vm.AddTodoFromWebAsync();
        }
    }
}
