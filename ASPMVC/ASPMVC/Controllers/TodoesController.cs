﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ASPMVC.Models;
using Microsoft.AspNet.Identity;

namespace ASPMVC.Controllers
{
    [Authorize(Roles = "admin")]
    public class TodoesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Todoes
        public ActionResult Index(string searchString, string sortBy)
        {
            var currentUserId = User.Identity.GetUserId();
            //Tylko zadania danego usera:
            var myTodos = db.Todos.Where(t => t.OwnerId == currentUserId);
            if (!String.IsNullOrEmpty(searchString))
            {
                myTodos = myTodos.Where(t => t.Name.Contains(searchString));
            }
            if (!String.IsNullOrEmpty(sortBy))
            {
                switch (sortBy)
                {
                    case ("Name"):
                        myTodos = myTodos.OrderBy(t=>t.Name);
                        break;
                    case ("Date"):
                        myTodos = myTodos.OrderBy(t => t.Beginning);
                        break;
                    default:
                        break;
                }
            }
            return View(myTodos.ToList());
        }

        // GET: Todoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Todo todo = db.Todos.Find(id);
            if (todo == null)
            {
                return HttpNotFound();
            }
            return View(todo);
        }

        // GET: Todoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Todoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Beginning,End,Description,NumberOfParticipants")] Todo todo)
        {
            if (ModelState.IsValid)
            {
                var currentUserId = User.Identity.GetUserId();
                todo.OwnerId = currentUserId;
                db.Todos.Add(todo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(todo);
        }

        // GET: Todoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Todo todo = db.Todos.Find(id);
            if (todo == null)
            {
                return HttpNotFound();
            }
            return View(todo);
        }

        // POST: Todoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Beginning,End,Description,NumberOfParticipants")] Todo todo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(todo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(todo);
        }

        // GET: Todoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Todo todo = db.Todos.Find(id);
            if (todo == null)
            {
                return HttpNotFound();
            }
            return View(todo);
        }

        // POST: Todoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Todo todo = db.Todos.Find(id);
            db.Todos.Remove(todo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //Ta metoda powinna być w api kontrolerze
        [HttpPost]
        public ActionResult DeleteTodo(int id)
        {
            Todo todo = db.Todos.Find(id);
            db.Todos.Remove(todo);
            db.SaveChanges();
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        //Ta metoda powinna być w api kontrolerze
        [HttpPost]
        public ActionResult DuplicateTodo(int id)
        {
            Todo todo = db.Todos.Find(id);
            Todo newTodo = new Todo
            {
                Name = todo.Name + "-copy",
                Beginning = todo.Beginning,
                End = todo.End,
                OwnerId = todo.OwnerId,
                Description = todo.Description,
                NumberOfParticipants = todo.NumberOfParticipants
            };

            db.Todos.Add(newTodo);
            db.SaveChanges();
            return new JsonResult
            {
                Data=newTodo
            };
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
