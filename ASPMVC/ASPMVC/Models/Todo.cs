﻿using ASPMVC.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ASPMVC.Models
{
    public class Todo
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public DateTime Beginning { get; set; }

        [EndLaterThanStart("Beginning")]
        public DateTime End { get; set; }
        public string Description { get; set; }
        [DisplayName("Participants")]
        [Range(0,30, ErrorMessage ="Please enter number between 0 and 30")]
        public int NumberOfParticipants { get; set; }

        public string OwnerId { get; set; }
        [NotMapped]
        public virtual ApplicationUser Owner { get; set; }
    }
}