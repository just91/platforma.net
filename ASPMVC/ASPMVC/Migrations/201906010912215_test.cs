namespace ASPMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Todoes", "OwnerId", "dbo.AspNetUsers");
            DropIndex("dbo.Todoes", new[] { "OwnerId" });
            AlterColumn("dbo.Todoes", "OwnerId", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Todoes", "OwnerId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Todoes", "OwnerId");
            AddForeignKey("dbo.Todoes", "OwnerId", "dbo.AspNetUsers", "Id");
        }
    }
}
