namespace ASPMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedOwnerToTodo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Todoes", "OwnerId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Todoes", "OwnerId");
            AddForeignKey("dbo.Todoes", "OwnerId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Todoes", "OwnerId", "dbo.AspNetUsers");
            DropIndex("dbo.Todoes", new[] { "OwnerId" });
            DropColumn("dbo.Todoes", "OwnerId");
        }
    }
}
