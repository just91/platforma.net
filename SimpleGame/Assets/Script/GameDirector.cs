﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDirector : MonoBehaviour {

    public PlayerCollisions PlayerCol;
    public PickupSpawner PickupSp;
    public ScoreCounter ScoreCounter;

	// Use this for initialization
	void Start () {
        PlayerCol.OnPickupCollision += ReactToCollision;
	}

    private void ReactToCollision()
    {
        PickupSp.SpawnPickup();
        ScoreCounter.AddPointsForPickup();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
