﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplayer : MonoBehaviour {

    public ScoreCounter ScoreC;
    private Text textComp;
	// Use this for initialization
	void Start () {
        ScoreC.ChangedScore += DisplayNewScore;
        textComp = this.GetComponent<Text>();
    }
	
    private void DisplayNewScore()
    {
        textComp.text = "score: " + ScoreC.Score;
    }
}
