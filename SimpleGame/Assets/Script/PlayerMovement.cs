﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    public float maxX;
    public float maxZ;
    public float Speed;
    public float RotateSpeed;
    public event Action IsMoving;
    public event Action IsNotMoving;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        //Vector3 newPosition = this.transform.position + Time.deltaTime*Speed*new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        //float x = Mathf.Min(newPosition.x,maxX);
        //x = Mathf.Max(x, -maxX);
        //float z = Mathf.Min(newPosition.z, maxZ);
        //z = Mathf.Max(z, -maxZ);

        //this.transform.position = new Vector3(x, 0.5f, z);

        this.transform.Rotate(0, Time.deltaTime*RotateSpeed*Input.GetAxis("Horizontal"), 0);
        Debug.Log(this.transform.forward);
        Vector3 newPosition = this.transform.position + this.transform.forward * Time.deltaTime * Speed * Input.GetAxis("Vertical");
        newPosition = newPosition + this.transform.right * Time.deltaTime * Speed * Input.GetAxis("Horizontal");
        float x = Mathf.Min(newPosition.x, maxX);
        x = Mathf.Max(x, -maxX);
        float z = Mathf.Min(newPosition.z, maxZ);
        z = Mathf.Max(z, -maxZ);
        this.transform.position = new Vector3(x, 0.0f, z);
        if (Input.GetAxis("Horizontal")!=0|| Input.GetAxis("Vertical")!=0)
        {
            if (IsMoving!=null)
            {
                IsMoving();
            }
        }
        else
        {
            if (IsNotMoving != null)
            {
                IsNotMoving();
            }
        }
    }
}
