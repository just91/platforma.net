﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawner : MonoBehaviour {

    public Transform PickupPrefab;
	public void SpawnPickup()
    {
        Instantiate(PickupPrefab, new Vector3(Random.Range(-4.5f,4.5f), 0.5f, Random.Range(-4.5f,4.5f)), Quaternion.identity);
    }
}
