﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour {
    public PlayerMovement PlayerMov;
    public Animator playerAnimator;
	// Use this for initialization
	void Start () {
        PlayerMov.IsMoving += Walk;
        PlayerMov.IsNotMoving += Idle;
	}

    private void Idle()
    {
        playerAnimator.SetBool("IsInputNonZero", false);

    }

    private void Walk()
    {
        playerAnimator.SetBool("IsInputNonZero", true);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
