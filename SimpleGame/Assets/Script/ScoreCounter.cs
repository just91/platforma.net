﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ScoreCounter : MonoBehaviour {
    public int Score { get; private set; }
    private int pointsForPickup =50;
    public event Action ChangedScore;

    public void AddPointsForPickup()
    {
        Score += pointsForPickup;
        if (ChangedScore!=null)
        {
            ChangedScore();
        }
    }
}
