using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ConsoleApp.Models;
using Newtonsoft.Json;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {

            // The code provided will print ‘Hello World’ to the console.
            // Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.
            //ctrl+e+d =>formatuje kod
            //ctrl+r refactor
            Console.WriteLine("Hello World!");
            Console.WriteLine("Do you want to exit? Y/N");

            string answer = Console.ReadLine();
            if (answer.ToUpper() == "Y")
            {
                return;
            }
            while (answer.ToUpper() != "N")
            {
                Console.WriteLine("Do you want to exit? Y/N");
                if (answer.ToUpper() == "Y")
                {
                    return;
                }
                answer = Console.ReadLine();
            }
            Console.ReadLine();

            //Konstruktory
            Todo todo = new Todo();
            Todo todo2 = new Todo("todo constructed with name");
            Console.WriteLine(todo.Name);
            Console.WriteLine(todo2.Name);
            Console.ReadLine();

            //DateTime
            todo.Beginning = new DateTime(2019, 04, 27, 10, 0, 0);
            todo.End = todo.Beginning.AddMinutes(90);

            //Skrócona inicjalizacja obiektu
            Place place = new Place { Name = "Poznań", ZipCode = 61614, Latitude = 52.467226f, Longitude = 16.927228f };
            todo.Place = place;

            //Typy wartości i typy referencyjne
            int valueType = 3;
            ChangeValueType(ref valueType);
            Console.WriteLine(valueType.ToString());
            Console.ReadLine();
            ChangeRefType(todo);
            Console.WriteLine(todo.Name);
            Console.ReadLine();

            //parsowanie stringa na int
            Console.WriteLine("Please enter zipcode in format 00000");
            string zipCodeAnswer = Console.ReadLine();
            int zipCode;
            while (!CheckIfZipCodeIsValid(zipCodeAnswer, out zipCode))
            {
                Console.WriteLine("Please enter zipcode in format 00000");
                zipCodeAnswer = Console.ReadLine();
            }
            Place newPlace = new Place { ZipCode = zipCode };

            //wyrażenia regularne
            Console.WriteLine("Enter beginning of your task in format dd/MM/yyyy");
            string begAnswer = Console.ReadLine();
            DateTime beginning;
            while (!CheckIfDateIsValid(begAnswer, out beginning))
            {
                Console.WriteLine("Enter beginning of your task in format dd/MM/yyyy");
                begAnswer = Console.ReadLine();
            }
            Console.WriteLine("Valid date.");

            //tablice, listy
            Todo[] todos = new Todo[3];
            todos[0] = new Todo("1st todo");
            todos[1] = new Todo("2nd todo");
            todos[2] = new Todo("3rd todo");

            List<Todo> todoList = new List<Todo>();
            Console.WriteLine("Before adding milion objectsToList");
            Console.ReadLine();

            //Garbage collector
            for (int i = 0; i < 1000000; i++)
            {
                Todo todoOnList = new Todo("task number: " + (i + 1).ToString());
                todoList.Add(todoOnList);
            }
            Console.WriteLine("Added milion objectsToList");
            Console.ReadLine();
            todoList.Clear();
            Console.ReadLine();
            GC.Collect();
            Console.ReadLine();

            //Przykłady linq
            List<string> names = todoList.Select(td => td.Name).ToList();
            List<string> namesWithOne = names.Where(name => name.Contains("1")).ToList();

            //Zapisywanie, zczytywanie z pliku ->pliki są w folderze Bin
            Todo todoInFile = new Todo() { Beginning = beginning, End = beginning.AddDays(2), Place = place };
            using (StreamWriter writer = new StreamWriter("firstToDo.txt"))
            {
                writer.WriteLine("Name: " + todoInFile.Name);
                writer.WriteLine("TimeBeginning: " + todoInFile.Beginning.ToString());
                writer.WriteLine("TimeEnd: " + todoInFile.End.ToString());
                writer.WriteLine("Place: " + todoInFile.Place.ToString());
            }

            //Newtonsoft.Json -> pakiet z NUGET
            //Konwersja obiektu na json
            string jsonTodo = JsonConvert.SerializeObject(todoInFile);
            using (StreamWriter writer = new StreamWriter("secondToDo.txt"))
            {
                writer.WriteLine(jsonTodo);
            }
            Console.WriteLine("Written a file.");
            Console.ReadLine();
        }

        private static bool CheckIfDateIsValid(string begAnswer, out DateTime beginning)
        {
            Regex regDate;
            regDate = new Regex(@"^([0]?[1-9]|[1|2][0-9]|[3][0|1])[./-]([0]?[1-9]|[1][0-2])[./-]([0-9]{4})$");
            bool isDate = regDate.IsMatch(begAnswer);
            if (!isDate)
            {
                beginning = DateTime.Now;
                return false;
            }
            string dayString = begAnswer.Substring(0, 2);
            string monthString = begAnswer.Substring(3, 2);
            string yearString = begAnswer.Substring(6, 4);
            try
            {
                beginning = new DateTime(Int32.Parse(yearString), Int32.Parse(monthString), Int32.Parse(dayString));
            }
            catch (ArgumentOutOfRangeException e)
            {
                beginning = DateTime.Now;
                return false;
            }
            return isDate;
        }

        static void ChangeValueType(ref int valType)
        {
            valType++;
            return;
        }

        static void ChangeRefType(Todo todo)
        {
            todo.Name = "ChangedName";
        }

        static bool CheckIfZipCodeIsValid(string answer, out int zipCode)
        {
            bool conversionSucceed = Int32.TryParse(answer, out zipCode);
            bool isFiveDigitInt = zipCode >= 10000 && zipCode <= 99999;
            return conversionSucceed && isFiveDigitInt;

        }
    }
}
