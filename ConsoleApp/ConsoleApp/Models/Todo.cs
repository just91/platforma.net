﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.Models
{
    class Todo
    {
        //prop+tab+tab => new property
        public string Name { get; set; }
        public DateTime Beginning { get; set; }
        public DateTime End { get; set; }
        public string Description { get; set; }
        public int NumberOfParticipants { get; set; }
        public Place Place { get; set; }
        public Todo()
        {
            Name = "new todo";
        }

        public Todo(string name)
        {
            Name = name;
        }
    }
}
