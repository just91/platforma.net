﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp.Models
{
    class Place
    {
        public string Name { get; set; }
        public int ZipCode { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }

        public override string ToString()
        {
            return "name: " + Name + ", Zipcode: " + ZipCode.ToString();
        }
    }
}
