﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplikacjaBezMVVM.Models
{
    public class Todo
    {
        public string Name { get; set; }
        public DateTime Beginning { get; set; }
        public int NumberOfParticipants { get; set; }

        public Todo(string name, int number, DateTime beg)
        {
            Name = name;
            Beginning = beg;
            NumberOfParticipants = number;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
