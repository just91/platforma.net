﻿using AplikacjaBezMVVM.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AplikacjaBezMVVM
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<Todo> Todos { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            Todos = new ObservableCollection<Todo>();
            TodosListView.ItemsSource = Todos;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            string name = InputTodoName.Text;
            string numberString = InputTodoNumberOfParticipants.Text;
            int number;
            if (!Int32.TryParse(numberString, out number))
            {
                return;
            }
            DateTime beginning = (DateTime)InputTodoBeginning.SelectedDate;
            Todos.Add(new Todo(name,number,beginning));
            InputTodoName.Text ="";
            InputTodoNumberOfParticipants.Text ="";
            InputTodoBeginning.SelectedDate = DateTime.Now;
        }
    }
}
